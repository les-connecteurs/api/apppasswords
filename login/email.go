package login

import (
	"context"
	"database/sql"
	"log"
	"strings"

	"connecteu.rs/api/apppasswords/proto"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func countResponse(ctx context.Context, db *sql.DB, query, search string) (int, error) {
	var count int

	row := db.QueryRowContext(ctx, query, search)
	err := row.Scan(&count)
	if err != nil {
		return 0, err
	}

	return count, nil
}

func CheckLoginEmail(ctx context.Context, db *sql.DB, req *proto.CheckLoginRequest) (*proto.CheckLoginResponse, error) {
	// TODO: drop this
	ctx, span := tracer.Start(ctx, "CheckLoginEmail")
	defer span.End()

	var count int
	var err error

	// force use of "@"
	if !strings.Contains(req.Username, "@") {
		return nil, status.Errorf(codes.InvalidArgument, "email address must include the '@' character")
	}

	// disallow use of aliases
	if strings.Contains(req.Username, "+") {
		return nil, status.Errorf(codes.InvalidArgument, "email address cannot be an alias")
	}

	// check if a user is using this email address
	err = tracer.WithSpan(ctx, "CountExistingEmails", func(ctx context.Context) error {
		var err error
		count, err = countResponse(ctx, db, `
      SELECT COUNT(*)
        FROM public.app_passwords
        WHERE type = 'email'
          AND username = $1
    `, req.Username)
		return err
	})
	if err != nil {
		log.Println(err)
		return nil, status.Errorf(codes.Internal, "something went wrong while fetching data from database")
	}

	// this email address is taken by an existing user
	if count > 0 {
		return &proto.CheckLoginResponse{
			State: proto.LoginState_LOGIN_STATE_TAKEN,
		}, nil
	}

	// check if a service is using this email address
	err = tracer.WithSpan(ctx, "CountExistingServices", func(ctx context.Context) error {
		var err error
		count, err = countResponse(ctx, db, `
      SELECT COUNT(*)
        FROM public.email_services
        WHERE email = $1
    `, req.Username)
		return err
	})
	if err != nil {
		log.Println(err)
		return nil, status.Errorf(codes.Internal, "something went wrong while fetching data from database")
	}

	// this email address is used by a service
	if count > 0 {
		return &proto.CheckLoginResponse{
			State: proto.LoginState_LOGIN_STATE_RESERVED,
		}, nil
	}

	// this email address is available
	return &proto.CheckLoginResponse{
		State: proto.LoginState_LOGIN_STATE_AVAILABLE,
	}, nil
}
