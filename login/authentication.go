package login

import (
	"context"
	"database/sql"
	"log"

	"go.opentelemetry.io/otel/api/global"
	"go.opentelemetry.io/otel/api/kv"
	"go.opentelemetry.io/otel/api/trace"
	"golang.org/x/crypto/bcrypt"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"connecteu.rs/api/apppasswords/proto"
)

var tracer = global.Tracer("connecteu.rs/api/apppasswords/login")

func CheckAuthentication(ctx context.Context, db *sql.DB, accountType, username, password string) (*proto.AuthenticateResponse, error) {
	ctx, span := tracer.Start(
		ctx, "CheckAuthentication",
		trace.WithAttributes(
			kv.String("account_type", accountType),
			kv.String("username", username)),
	)
	defer span.End()

	var err error
	var rows *sql.Rows

	err = tracer.WithSpan(ctx, "QueryContext", func(ctx context.Context) error {
		var err error
		rows, err = db.QueryContext(ctx, `
      SELECT password
        FROM public.app_passwords
        WHERE type = $1
          AND username = $2
    `, accountType, username)
		return err
	})
	defer rows.Close()

	if err != nil {
		log.Println(err)
		return nil, status.Errorf(codes.Internal, "something went wrong while fetching data from database")
	}

	for rows.Next() {
		span.AddEvent(ctx, "next_row")
		var hash string

		if err = rows.Scan(&hash); err != nil {
			log.Println(err)
			return nil, status.Errorf(codes.Internal, "something went wrong while fetching data from database")
		}

		err = tracer.WithSpan(ctx, "CompareHashAndPassword", func(ctx context.Context) error {
			return bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
		})
		if err == nil {
			return &proto.AuthenticateResponse{
				Success: true,
			}, nil
		}
	}

	return &proto.AuthenticateResponse{
		Success: false,
	}, nil
}
