package login

import (
	"context"
	"database/sql"
	"log"

	"connecteu.rs/api/apppasswords/proto"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func CheckLoginMatrix(ctx context.Context, db *sql.DB, req *proto.CheckLoginRequest) (*proto.CheckLoginResponse, error) {
	// check if a user is using this username
	count, err := countResponse(ctx, db, `
    SELECT COUNT(*)
      FROM public.app_passwords
      WHERE type = 'matrix'
        AND username = $1
  `, req.Username)
	if err != nil {
		log.Println(err)
		return nil, status.Errorf(codes.Internal, "something went wrong while fetching data from database")
	}

	// this username is taken by an existing user
	if count > 0 {
		return &proto.CheckLoginResponse{
			State: proto.LoginState_LOGIN_STATE_TAKEN,
		}, nil
	}

	// this username is available
	return &proto.CheckLoginResponse{
		State: proto.LoginState_LOGIN_STATE_AVAILABLE,
	}, nil
}
