package server

import (
	"context"
	"log"

	"go.opentelemetry.io/otel/api/global"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"connecteu.rs/api/apppasswords/auth"
	"connecteu.rs/api/apppasswords/passwords"
	"connecteu.rs/api/apppasswords/proto"
	"connecteu.rs/api/apppasswords/utils"
)

var passwordsTracer = global.Tracer("connecteu.rs/api/apppasswords/server/apppasswords")

// AppPasswordsAPIServer is the server API for AppPasswordsAPI service
type AppPasswordsAPIServer struct {
	auth.Auth
	*proto.UnimplementedAppPasswordsAPIServer
}

// GeneratePassword is used to generate a new application password
func (s AppPasswordsAPIServer) GeneratePassword(ctx context.Context, req *proto.GeneratePasswordRequest) (*proto.GeneratePasswordResponse, error) {
	ctx, span := passwordsTracer.Start(ctx, "GeneratePassword")
	defer span.End()

	var err error

	// openid scope is needed for userinfo
	introspection, err := s.Auth.Introspect(ctx, []string{"apppasswords:write", "openid"})
	if err != nil {
		return &proto.GeneratePasswordResponse{}, err
	}

	err = auth.RequiresAuthentication(introspection)
	if err != nil {
		return &proto.GeneratePasswordResponse{}, err
	}

	err = s.Auth.LoadUserinfo(ctx, introspection)
	if err != nil {
		return &proto.GeneratePasswordResponse{}, err
	}

	db, err := utils.DatabaseInstance()
	if err != nil {
		return &proto.GeneratePasswordResponse{}, status.Errorf(codes.Internal, "unable to get database instance")
	}

	return passwords.Generate(ctx, introspection, db, req)
}

// DeletePassword is used to delete a specific application password
func (s AppPasswordsAPIServer) DeletePassword(ctx context.Context, req *proto.DeletePasswordRequest) (*proto.DeletePasswordResponse, error) {
	ctx, span := passwordsTracer.Start(ctx, "DeletePassword")
	defer span.End()

	var err error

	introspection, err := s.Auth.Introspect(ctx, []string{"apppasswords:write"})
	if err != nil {
		return &proto.DeletePasswordResponse{}, err
	}

	err = auth.RequiresAuthentication(introspection)
	if err != nil {
		return &proto.DeletePasswordResponse{}, err
	}

	db, err := utils.DatabaseInstance()
	if err != nil {
		log.Println(err)
		return &proto.DeletePasswordResponse{}, status.Errorf(codes.Internal, "unable to get database instance")
	}

	return passwords.Delete(ctx, introspection, db, req)
}

// ListPasswords is used to list all application passwords of the authenticated user
func (s AppPasswordsAPIServer) ListPasswords(ctx context.Context, req *proto.ListPasswordsRequest) (*proto.ListPasswordsResponse, error) {
	ctx, span := passwordsTracer.Start(ctx, "ListPasswords")
	defer span.End()

	var err error

	introspection, err := s.Auth.Introspect(ctx, []string{"apppasswords:read"})
	if err != nil {
		return &proto.ListPasswordsResponse{}, err
	}

	err = auth.RequiresAuthentication(introspection)
	if err != nil {
		return &proto.ListPasswordsResponse{}, err
	}

	db, err := utils.DatabaseInstance()
	if err != nil {
		log.Println(err)
		return &proto.ListPasswordsResponse{}, status.Errorf(codes.Internal, "unable to get database instance")
	}

	return passwords.List(ctx, introspection, db, req)
}
