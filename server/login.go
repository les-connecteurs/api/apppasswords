package server

import (
	"context"

	"go.opentelemetry.io/otel/api/global"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"connecteu.rs/api/apppasswords/auth"
	"connecteu.rs/api/apppasswords/login"
	"connecteu.rs/api/apppasswords/proto"
	"connecteu.rs/api/apppasswords/utils"
)

var loginTracer = global.Tracer("connecteu.rs/api/apppasswords/server/login")

// LoginAPIServer is the server API for LoginAPI service
type LoginAPIServer struct {
	auth.Auth
	*proto.UnimplementedLoginAPIServer
}

// Authenticate is used to check if credentials are valid
func (s LoginAPIServer) Authenticate(ctx context.Context, req *proto.AuthenticateRequest) (*proto.AuthenticateResponse, error) {
	ctx, span := loginTracer.Start(ctx, "Authenticate")
	defer span.End()

	if len(req.Username) == 0 {
		return nil, status.Errorf(codes.InvalidArgument, "username is empty")
	}

	db, err := utils.DatabaseInstance()
	if err != nil {
		return nil, status.Errorf(codes.Internal, "unable to get database instance")
	}

	if req.Type == proto.LoginType_LOGIN_TYPE_EMAIL {
		span.SetAttribute("login_type", "email")
		return login.CheckAuthentication(ctx, db, "email", req.Username, req.Password)
	}

	if req.Type == proto.LoginType_LOGIN_TYPE_MATRIX {
		span.SetAttribute("login_type", "matrix")
		return login.CheckAuthentication(ctx, db, "matrix", req.Username, req.Password)
	}

	return nil, status.Errorf(codes.InvalidArgument, "Invalid login type")
}

// CheckLogin is used to check if a login is available or already taken
func (s LoginAPIServer) CheckLogin(ctx context.Context, req *proto.CheckLoginRequest) (*proto.CheckLoginResponse, error) {
	ctx, span := loginTracer.Start(ctx, "CheckLogin")
	defer span.End()

	if len(req.Username) == 0 {
		return nil, status.Errorf(codes.InvalidArgument, "username is empty")
	}

	db, err := utils.DatabaseInstance()
	if err != nil {
		return nil, status.Errorf(codes.Internal, "unable to get database instance")
	}

	if req.Type == proto.LoginType_LOGIN_TYPE_EMAIL {
		return login.CheckLoginEmail(ctx, db, req)
	}

	if req.Type == proto.LoginType_LOGIN_TYPE_MATRIX {
		return login.CheckLoginMatrix(ctx, db, req)
	}

	return nil, status.Errorf(codes.InvalidArgument, "invalid login type")
}
