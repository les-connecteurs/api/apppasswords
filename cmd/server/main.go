package main

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/gorilla/mux"
	gwruntime "github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	muxtrace "go.opentelemetry.io/contrib/instrumentation/github.com/gorilla/mux"
	"go.opentelemetry.io/contrib/instrumentation/runtime"
	"go.opentelemetry.io/otel/api/global"
	"go.opentelemetry.io/otel/api/kv"
	"go.opentelemetry.io/otel/api/propagation"
	"go.opentelemetry.io/otel/api/standard"
	"go.opentelemetry.io/otel/api/trace"
	"go.opentelemetry.io/otel/exporters/otlp"
	"go.opentelemetry.io/otel/instrumentation/grpctrace"
	"go.opentelemetry.io/otel/sdk/metric/controller/push"
	"go.opentelemetry.io/otel/sdk/metric/selector/simple"
	"go.opentelemetry.io/otel/sdk/resource"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"

	"connecteu.rs/api/apppasswords/auth"
	"connecteu.rs/api/apppasswords/proto"
	"connecteu.rs/api/apppasswords/server"
	"connecteu.rs/api/apppasswords/utils"
)

func initProvider() func() {
	value, ok := os.LookupEnv("OTLP_COLLECTOR")

	if !ok {
		log.Println("Starting without collector")
		return func() {}
	}

	exp, err := otlp.NewExporter(
		otlp.WithAddress(value),
		otlp.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}

	tp, err := sdktrace.NewProvider(
		sdktrace.WithConfig(sdktrace.Config{DefaultSampler: sdktrace.AlwaysSample()}),
		sdktrace.WithResource(resource.New(
			kv.Key(standard.ServiceNameKey).String("apppasswords"),
		)),
		sdktrace.WithSyncer(exp),
	)
	if err != nil {
		log.Fatal(err)
	}

	pusher := push.New(
		simple.NewWithExactDistribution(),
		exp,
		push.WithPeriod(2*time.Second),
	)

	b3 := trace.B3{}
	tc := trace.TraceContext{}
	global.SetTraceProvider(tp)
	global.SetMeterProvider(pusher.Provider())
	global.SetPropagators(propagation.New(
		propagation.WithInjectors(b3, tc),
		propagation.WithExtractors(b3, tc),
	))
	pusher.Start()

	return func() {
		exp.Stop()
		pusher.Stop()
	}
}

func main() {
	ctx := context.Background()

	stop := initProvider()
	defer stop()

	tracer := global.Tracer("connecteu.rs/api/apppasswords")
	meter := global.Meter("connecteu.rs/api/apppasswords")

	if err := runtime.Start(meter, time.Second); err != nil {
		log.Fatalf("could not init runtime instrumentation: %v", err)
	}

	log.Println("App passwords service starting…")

	introspectionEndpoint := utils.GetEnv("INTROSPECTION_ENDPOINT", "http://localhost/oauth2/introspect")
	userinfoEndpoint := utils.GetEnv("USERINFO_ENDPOINT", "http://localhost/userinfo")

	socket, err := net.Listen("tcp", ":3000")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	u, err := url.Parse(introspectionEndpoint)
	if err != nil {
		log.Fatalf("Invalid introspection URL: %s", err)
	}

	u2, err := url.Parse(userinfoEndpoint)
	if err != nil {
		log.Fatalf("Invalid userinfo URL: %s", err)
	}

	a := auth.New(*u, *u2)
	loginServer := server.LoginAPIServer{Auth: a}
	passwordsServer := server.AppPasswordsAPIServer{Auth: a}

	grpcServer := grpc.NewServer(
		grpc.UnaryInterceptor(grpctrace.UnaryServerInterceptor(tracer)),
		grpc.StreamInterceptor(grpctrace.StreamServerInterceptor(tracer)),
	)
	reflection.Register(grpcServer)
	proto.RegisterLoginAPIServer(grpcServer, loginServer)
	proto.RegisterAppPasswordsAPIServer(grpcServer, passwordsServer)

	r := mux.NewRouter()

	mux := gwruntime.NewServeMux()
	proto.RegisterAppPasswordsAPIHandlerServer(ctx, mux, passwordsServer)

	r.HandleFunc("/readyz", func(w http.ResponseWriter, r *http.Request) {
		db, err := utils.DatabaseInstance()
		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		}

		err = db.PingContext(r.Context())
		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		}

		fmt.Fprint(w, "ok")
	})

	r.HandleFunc("/healthz", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, "ok")
	})

	r.PathPrefix("/").Handler(mux)
	r.Use(muxtrace.Middleware("apppasswords-rest-api"))

	srv := &http.Server{
		Addr:         ":3001",
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      r,
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil {
			log.Println(err)
		}
	}()

	go func() {
		if err := grpcServer.Serve(socket); err != nil {
			log.Println(err)
		}
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	signal.Notify(c, syscall.SIGTERM)
	<-c

	log.Println("Graceful stop…")
	wg := sync.WaitGroup{}
	wg.Add(3)

	ctx, cancel := context.WithTimeout(ctx, time.Second*15)
	defer cancel()

	go func() {
		srv.Shutdown(ctx)
		wg.Done()
	}()
	go func() {
		grpcServer.GracefulStop()
		wg.Done()
	}()
	go func() {
		utils.DatabaseClose()
		wg.Done()
	}()

	wg.Wait()
	log.Println("Exit")
}
