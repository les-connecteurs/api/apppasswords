module connecteu.rs/api/apppasswords

go 1.14

require (
	github.com/DataDog/sketches-go v0.0.1 // indirect
	github.com/golang/protobuf v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.0.0-beta.3
	github.com/lib/pq v1.7.1
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	go.opentelemetry.io/contrib/instrumentation/github.com/gorilla/mux v0.9.0
	go.opentelemetry.io/contrib/instrumentation/runtime v0.9.0
	go.opentelemetry.io/otel v0.9.0
	go.opentelemetry.io/otel/exporters/otlp v0.9.0
	golang.org/x/crypto v0.0.0-20200709230013-948cd5f35899
	golang.org/x/net v0.0.0-20200707034311-ab3426394381
	golang.org/x/sys v0.0.0-20200722175500-76b94024e4b6 // indirect
	golang.org/x/text v0.3.3 // indirect
	google.golang.org/genproto v0.0.0-20200722002428-88e341933a54 // indirect
	google.golang.org/grpc v1.30.0
	google.golang.org/grpc/cmd/protoc-gen-go-grpc v0.0.0-20200721210703-a5a36bd3f0bb
	google.golang.org/protobuf v1.25.0 // indirect
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
	gopkg.in/guregu/null.v3 v3.5.0
	gopkg.in/yaml.v2 v2.3.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200605160147-a5ece683394c // indirect
)
