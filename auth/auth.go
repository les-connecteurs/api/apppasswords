package auth

import (
	"context"
	"encoding/json"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"go.opentelemetry.io/otel/api/global"
	"go.opentelemetry.io/otel/api/kv"
	"go.opentelemetry.io/otel/api/trace"
	"go.opentelemetry.io/otel/instrumentation/httptrace"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

var tracer = global.Tracer("connecteu.rs/api/apppasswords/auth")

type contextKey int

const keyIntrospectionResult contextKey = iota

// IntrospectionResult stores results from introspection
type IntrospectionResult struct {
	Active bool    `json:"active"`
	Sub    *string `json:"sub,omitempty"`
}

// UserInfo stores user informations fetched from userinfo endpoint
type UserInfo struct {
	Email *string `json:"email"`
	Sub   string  `json:"sub"`
}

// IntrospectionData returns introspection results, additionl user informations and the token
type IntrospectionData struct {
	Result   *IntrospectionResult
	Userinfo *UserInfo
	Token    *string
}

// Auth contains introspection and userinfo endpoints and instance of http client
type Auth struct {
	introspectionEndpoint url.URL
	userinfoEndpoint      url.URL
	*http.Client
}

// New creates a new Auth instance
func New(endpoint url.URL, userinfoURL url.URL) Auth {
	return Auth{
		introspectionEndpoint: endpoint,
		userinfoEndpoint:      userinfoURL,
		Client:                http.DefaultClient,
	}
}

// extractBearer extracts bearer token from authorization header
func extractBearer(authorization []string) string {
	if len(authorization) < 1 {
		return ""
	}

	return strings.TrimPrefix(authorization[0], "Bearer ")
}

// Introspect return the result of the introspection of the token got from the context and an error if any
func (a Auth) Introspect(ctx context.Context, requiredScopes []string) (*IntrospectionData, error) {
	ctx, span := tracer.Start(ctx, "Introspect",
		trace.WithAttributes(kv.Array("required_scopes", requiredScopes)))
	defer span.End()

	ctx, cancel := context.WithTimeout(ctx, 2*time.Second)
	defer cancel()

	var err error

	intro := IntrospectionData{
		Result:   nil,
		Token:    nil,
		Userinfo: nil,
	}

	// extract token from metadata and execute the handler if none was present
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return &intro, nil
	}

	token := extractBearer(md["authorization"])
	if token == "" {
		return &intro, nil
	}
	intro.Token = &token

	// build the introspection request
	data := url.Values{}
	data.Add("token", token)
	data.Add("token_type_hints", "access_token")
	data.Add("scope", strings.Join(requiredScopes, " "))
	body := data.Encode()

	r, err := http.NewRequest("POST", a.introspectionEndpoint.String(), strings.NewReader(body))
	if err != nil {
		log.Println(err)
		return nil, status.Errorf(codes.Internal, "could not generate introspection request")
	}

	r.Header.Add("Accept", "application/json")
	r.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	r.Header.Add("Content-Length", strconv.Itoa(len(body)))
	r.Header.Add("User-Agent", "apppasswords")

	ctx, r = httptrace.W3C(ctx, r)
	httptrace.Inject(ctx, r)

	// and execute it
	resp, err := a.Client.Do(r)
	if err != nil {
		log.Println(err)
		return nil, status.Errorf(codes.Internal, "could not execute introspection request")
	}

	if resp.StatusCode >= 400 {
		log.Println("introspection http error code:", resp.StatusCode)
		return nil, status.Errorf(codes.Internal, "unexpected introspection response")
	}

	// decode the response
	intro.Result = &IntrospectionResult{}
	err = json.NewDecoder(resp.Body).Decode(intro.Result)
	if err != nil {
		log.Println(err)
		return nil, status.Errorf(codes.Internal, "could not decode introspection response")
	}

	return &intro, nil
}

// RequiresAuthentication returns nil if the user is authenticated or an error if it not the case
func RequiresAuthentication(introspection *IntrospectionData) error {
	if introspection == nil || introspection.Result == nil {
		return status.Errorf(codes.PermissionDenied, "user is not authenticated")
	}

	if !introspection.Result.Active || introspection.Result.Sub == nil {
		return status.Errorf(codes.PermissionDenied, "permission denied, session may be expired")
	}

	return nil
}

// LoadUserinfo loads user informations and store it in the introspection data
func (a Auth) LoadUserinfo(ctx context.Context, i *IntrospectionData) error {
	ctx, span := tracer.Start(ctx, "LoadUserinfo")
	defer span.End()

	ctx, cancel := context.WithTimeout(ctx, 2*time.Second)
	defer cancel()

	r, err := http.NewRequest("GET", a.userinfoEndpoint.String(), nil)
	if err != nil {
		log.Println(err)
		return status.Errorf(codes.Internal, "could not generate userinfo request")
	}

	r.Header.Add("Accept", "application/json")
	r.Header.Add("User-Agent", "apppasswords")
	r.Header.Add("Authorization", "Bearer "+*i.Token)

	ctx, r = httptrace.W3C(ctx, r)
	httptrace.Inject(ctx, r)

	// and execute it
	resp, err := a.Client.Do(r)
	if err != nil {
		log.Println(err)
		return status.Errorf(codes.Internal, "could not execute userinfo request")
	}

	if resp.StatusCode >= 400 {
		return status.Errorf(codes.Internal, "unexpected userinfo response")
	}

	// decode the response
	i.Userinfo = &UserInfo{}
	err = json.NewDecoder(resp.Body).Decode(i.Userinfo)
	if err != nil {
		log.Println(err)
		return status.Errorf(codes.Internal, "could not decode userinfo response")
	}

	return nil
}
