# Application Passwords

To test locally, you can run:

```sh
make && make start
```

Swagger will be exposed at: http://localhost:3002.

HTTP API will be exposed at: http://localhost:3001.

You will need to pass an `Authorization` header.
For that, you can use the [ModHeader](https://bewisse.com/modheader/) extension for your browser or make requests using [Insomnia](https://insomnia.rest/) for example.

To update Bazel dependencies, you will need to use:

```sh
make update
```

To clean all generated files, you can run:

```sh
make clean
```

More infos: https://github.com/grpc-ecosystem/grpc-gateway
