workspace(name = "connecteu_rs_api_apppasswords")

load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")

# Fetch the rules
http_archive(
    name = "io_bazel_rules_go",
    sha256 = "2d536797707dd1697441876b2e862c58839f975c8fc2f0f96636cbd428f45866",
    urls = [
        "https://mirror.bazel.build/github.com/bazelbuild/rules_go/releases/download/v0.23.5/rules_go-v0.23.5.tar.gz",
        "https://github.com/bazelbuild/rules_go/releases/download/v0.23.5/rules_go-v0.23.5.tar.gz",
    ],
)

http_archive(
    name = "rules_proto",
    sha256 = "602e7161d9195e50246177e7c55b2f39950a9cf7366f74ed5f22fd45750cd208",
    strip_prefix = "rules_proto-97d8af4dc474595af3900dd85cb3a29ad28cc313",
    urls = [
        "https://mirror.bazel.build/github.com/bazelbuild/rules_proto/archive/97d8af4dc474595af3900dd85cb3a29ad28cc313.tar.gz",
        "https://github.com/bazelbuild/rules_proto/archive/97d8af4dc474595af3900dd85cb3a29ad28cc313.tar.gz",
    ],
)

http_archive(
    name = "io_bazel_rules_docker",
    sha256 = "4521794f0fba2e20f3bf15846ab5e01d5332e587e9ce81629c7f96c793bb7036",
    strip_prefix = "rules_docker-0.14.4",
    urls = ["https://github.com/bazelbuild/rules_docker/releases/download/v0.14.4/rules_docker-v0.14.4.tar.gz"],
)

# FIXME: using master because we're waiting for a release with #840
http_archive(
    name = "bazel_gazelle",
    sha256 = "fa597bb89d08b2af52e346d0e40e817caf951d9341e5fcbe01985cce6b8c7766",
    strip_prefix = "bazel-gazelle-eb1ed91bd4a8cc6a96d2b1f5f4bcd0f31661735a",
    urls = [
        "https://github.com/bazelbuild/bazel-gazelle/archive/eb1ed91bd4a8cc6a96d2b1f5f4bcd0f31661735a.tar.gz",
    ],
)

http_archive(
    name = "com_google_protobuf",
    sha256 = "9748c0d90e54ea09e5e75fb7fac16edce15d2028d4356f32211cfa3c0e956564",
    strip_prefix = "protobuf-3.11.4",
    urls = ["https://github.com/protocolbuffers/protobuf/archive/v3.11.4.zip"],
)

# Load the go rules and toolchains
load("@io_bazel_rules_go//go:deps.bzl", "go_register_toolchains", "go_rules_dependencies")

go_rules_dependencies()

go_register_toolchains()

# Load rules_proto
load("@rules_proto//proto:repositories.bzl", "rules_proto_dependencies", "rules_proto_toolchains")

rules_proto_dependencies()

rules_proto_toolchains()

load("@com_google_protobuf//:protobuf_deps.bzl", "protobuf_deps")

protobuf_deps()

# Load gazelle for go dependency management
load("@bazel_gazelle//:deps.bzl", "gazelle_dependencies")

gazelle_dependencies()

load(
    "@io_bazel_rules_docker//repositories:repositories.bzl",
    container_repositories = "repositories",
)

container_repositories()

load("@io_bazel_rules_docker//repositories:deps.bzl", container_deps = "deps")

container_deps()

load("@io_bazel_rules_docker//repositories:pip_repositories.bzl", "pip_deps")

pip_deps()

load("@io_bazel_rules_docker//container:container.bzl", "container_pull")

container_pull(
    name = "go_migrate_image",
    digest = "sha256:f258cc5fa2d03193e1c109ad1c941a7e710318115f509fcf3c89bd6a7e75605a",
    registry = "docker.io",
    repository = "migrate/migrate",
    tag = "v4.11.0",
)

container_pull(
    name = "swagger_ui_image",
    digest = "sha256:dbc214dd2976e85163a97791d396f57110109d5143eeb6bc06b5986444342f7b",
    registry = "docker.io",
    repository = "swaggerapi/swagger-ui",
    tag = "v3.30.0",
)

load(
    "@io_bazel_rules_docker//go:image.bzl",
    _go_image_repos = "repositories",
)

_go_image_repos()

# Gazelle generates a go_repositories macro with all the go dependencies from the go.mod/go.sum files.
# See BUILD.bazel

# To update those files, run
#   bazel run //:gazelle -- update-repos -prune -from_file go.mod -to_macro repositories.bzl%go_repositories

# gazelle:repository_macro repositories.bzl%go_repositories
load("repositories.bzl", "go_repositories")

go_repositories()
