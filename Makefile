BAZEL = bazel

.PHONY: generate
generate:
	$(BAZEL) run //:gazelle
	$(BAZEL) run //:images

.PHONY: start run dev
start:
	ibazel --run_output_interactive=false run //hack:docker-compose -- up -d
run: start
dev: start

.PHONY: logs
logs:
	$(BAZEL) run //hack:docker-compose -- logs

.PHONY: stop
stop:
	$(BAZEL) run //hack:docker-compose -- down

.PHONY: update
update:
	go mod tidy
	$(BAZEL) run //:gazelle -- update-repos -prune -from_file $(PWD)/go.mod -to_macro repositories.bzl%go_repositories

.PHONY: upgrade
upgrade:
	go mod tidy
	go get -u ./... || true
	$(BAZEL) run //:gazelle -- update-repos -prune -from_file $(PWD)/go.mod -to_macro repositories.bzl%go_repositories

.PHONY: clean
clean:
	$(BAZEL) clean
	$(RM) proto/*.pb.go proto/*.pb.gw.go
