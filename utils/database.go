package utils

import (
	"database/sql"
	"fmt"
	"time"

	// postgreSQL support
	_ "github.com/lib/pq"
	"gopkg.in/guregu/null.v3"
)

// AppPasswordsRow is the representation of a row of the `app_passwords` table
type AppPasswordsRow struct {
	ID          uint32
	UserID      string
	Name        string
	Description null.String
	Type        string
	Username    string
	Password    string
	ExpiresAt   null.Time
	CreatedAt   time.Time
}

var db *sql.DB

// DatabaseInstance returns a database instance
func DatabaseInstance() (*sql.DB, error) {
	var err error

	if db != nil {
		return db, nil
	}

	database := GetEnv("DB_NAME", "app")
	user := GetEnv("DB_USER", "root")
	password := GetEnv("DB_PASSWORD", "root")
	mode := GetEnv("DB_MODE", "disable")
	host := GetEnv("DB_HOST", "localhost")

	connectionString := fmt.Sprintf(
		"postgres://%s:%s@%s/%s?sslmode=%s",
		user, password, host, database, mode,
	)
	db, err = sql.Open("postgres", connectionString)

	return db, err
}

func DatabaseClose() error {
  if db == nil {
    return nil
  }

  return db.Close()
}
