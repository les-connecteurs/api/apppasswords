package utils

import "os"

// GetEnv get one environment variable, if not found will be set to `fallback` value
func GetEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}
