package passwords

import (
	"context"
	"database/sql"
	"log"

	"connecteu.rs/api/apppasswords/auth"
	"connecteu.rs/api/apppasswords/proto"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func Delete(ctx context.Context, introspection *auth.IntrospectionData, db *sql.DB, req *proto.DeletePasswordRequest) (*proto.DeletePasswordResponse, error) {
	ctx, span := tracer.Start(ctx, "Delete")
	defer span.End()

	var err error

	userID := introspection.Result.Sub

	err = tracer.WithSpan(ctx, "ExecContext", func(ctx context.Context) error {
		_, err := db.ExecContext(ctx, `
      DELETE
        FROM app_passwords
        WHERE user_id = $1 AND id = $2
    `, userID, req.Id)
		return err
	})
	if err != nil {
		log.Println(err)
		return nil, status.Errorf(codes.Internal, "could not delete password")
	}

	return &proto.DeletePasswordResponse{}, nil
}
