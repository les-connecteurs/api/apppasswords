package passwords

import (
	"context"
	"database/sql"
	"log"

	"connecteu.rs/api/apppasswords/auth"
	"connecteu.rs/api/apppasswords/proto"
	"connecteu.rs/api/apppasswords/utils"
	"github.com/golang/protobuf/ptypes"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func List(ctx context.Context, introspection *auth.IntrospectionData, db *sql.DB, req *proto.ListPasswordsRequest) (*proto.ListPasswordsResponse, error) {
	ctx, span := tracer.Start(ctx, "List")
	defer span.End()

	var rows *sql.Rows
	var passwords []*proto.ListPasswordItem

	userID := introspection.Result.Sub

	err := tracer.WithSpan(ctx, "QueryContext", func(ctx context.Context) error {
		var err error
		rows, err = db.QueryContext(ctx, `
      SELECT id, name, description, type, username, expires_at, created_at
        FROM app_passwords
        WHERE user_id = $1
        ORDER BY expires_at ASC, created_at DESC
    `, userID)
		return err
	})
	if err != nil {
		return &proto.ListPasswordsResponse{}, status.Errorf(codes.Internal, "something went wrong while fetching data from database")
	}

	defer rows.Close()

	for rows.Next() {
		span.AddEvent(ctx, "next_row")
		var userPassword utils.AppPasswordsRow
		var loginType proto.LoginType

		err := rows.Scan(
			&userPassword.ID,
			&userPassword.Name,
			&userPassword.Description,
			&userPassword.Type,
			&userPassword.Username,
			&userPassword.ExpiresAt,
			&userPassword.CreatedAt,
		)
		if err != nil {
			log.Println(err)
			return &proto.ListPasswordsResponse{}, status.Errorf(codes.Internal, "something went wrong while parsing data from database")
		}

		switch userPassword.Type {
		case "email":
			loginType = proto.LoginType_LOGIN_TYPE_EMAIL
		case "matrix":
			loginType = proto.LoginType_LOGIN_TYPE_MATRIX
		default:
			loginType = proto.LoginType_LOGIN_TYPE_INVALID
		}

		password := &proto.ListPasswordItem{
			Id:       userPassword.ID,
			Type:     loginType,
			Username: userPassword.Username,
			Name:     userPassword.Name,
		}

		if userPassword.Description.Valid {
			password.Description = userPassword.Description.String
		}

		if userPassword.ExpiresAt.Valid {
			exp, err := ptypes.TimestampProto(userPassword.ExpiresAt.Time)
			if err == nil {
				password.ExpirationTime = exp
			}
		}

		creation, err := ptypes.TimestampProto(userPassword.CreatedAt)
		if err == nil {
			password.CreationTime = creation
		}

		passwords = append(passwords, password)
	}

	err = rows.Err()
	if err != nil {
		log.Println(err)
		return &proto.ListPasswordsResponse{}, status.Errorf(codes.Internal, "something went wrong during application passwords list query")
	}

	return &proto.ListPasswordsResponse{
		Passwords: passwords,
	}, nil
}
