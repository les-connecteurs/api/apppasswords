package passwords

import (
	"context"
	"database/sql"
	"log"
	"strings"
	"time"

	"github.com/golang/protobuf/ptypes"
	"golang.org/x/crypto/bcrypt"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"gopkg.in/guregu/null.v3"

	"connecteu.rs/api/apppasswords/auth"
	"connecteu.rs/api/apppasswords/proto"
	"connecteu.rs/api/apppasswords/utils"
)

func Generate(ctx context.Context, introspection *auth.IntrospectionData, db *sql.DB, req *proto.GeneratePasswordRequest) (*proto.GeneratePasswordResponse, error) {
	ctx, span := tracer.Start(ctx, "Generate")
	defer span.End()

	var loginType string
	var err error
	var expiration null.Time
	var createdAt time.Time
	var passwordID uint32

	userID := introspection.Result.Sub

	span.SetAttribute("user_id", userID)

	// convert login type to a string
	switch req.Type {
	case proto.LoginType_LOGIN_TYPE_EMAIL:
		loginType = "email"
	case proto.LoginType_LOGIN_TYPE_MATRIX:
		loginType = "matrix"
	default:
		return nil, status.Errorf(codes.InvalidArgument, "invalid login type")
	}

	span.SetAttribute("login_type", loginType)

	name := strings.TrimSpace(req.Name)
	description := strings.TrimSpace(req.Description)
	username := strings.TrimSpace(req.Username)

	// handle expiration field
	exp, err := ptypes.Timestamp(req.ExpirationTime)
	if err != nil {
		expiration = null.TimeFromPtr(nil)
	} else {
		expiration = null.TimeFrom(exp)
	}

	if name == "" {
		return nil, status.Errorf(codes.InvalidArgument, "name is empty")
	}

	if username == "" {
		return nil, status.Errorf(codes.InvalidArgument, "username is empty")
	}

	// some specific checks in case it is a password for an email account
	if loginType == "email" {
		// force use of "@"
		if !strings.Contains(username, "@") {
			return nil, status.Errorf(codes.InvalidArgument, "email address must include the '@' character")
		}

		// disallow use of aliases
		if strings.Contains(username, "+") {
			return nil, status.Errorf(codes.InvalidArgument, "email address cannot be an alias")
		}

		// check if user owns this email address
		if introspection.Userinfo == nil || introspection.Userinfo.Email == nil {
			return nil, status.Errorf(codes.PermissionDenied, "missing email scope")
		}
		if username != *introspection.Userinfo.Email {
			return nil, status.Errorf(codes.PermissionDenied, "not owned email address")
		}
	}

	// generate password and corresponding hash
	password := utils.String(16)
	var hash []byte

	err = tracer.WithSpan(ctx, "GenerateFromPassword", func(ctx context.Context) error {
		var err error
		hash, err = bcrypt.GenerateFromPassword([]byte(password), 8)
		return err
	})
	if err != nil {
		log.Println(err)
		return nil, status.Errorf(codes.Internal, "could not generate a password at this time")
	}

	// insert it in database
	err = tracer.WithSpan(ctx, "QueryRowContext", func(ctx context.Context) error {
		return db.QueryRowContext(ctx, `
      INSERT INTO public.app_passwords (user_id, name, description, type, username, password, expires_at)
      VALUES ($1, $2, $3, $4, $5, $6, $7)
      RETURNING id, created_at
    `, userID, name, description, loginType, username, hash, expiration).Scan(&passwordID, &createdAt)
	})
	if err != nil {
		log.Println(err)
		return nil, status.Errorf(codes.Internal, "could not insert generated password in database")
	}

	creationTime, err := ptypes.TimestampProto(createdAt)
	if err != nil {
		log.Println(err)
		return nil, status.Errorf(codes.Internal, "could not fetch creation time")
	}

	return &proto.GeneratePasswordResponse{
		Id:             passwordID,
		Type:           req.Type,
		Username:       username,
		Password:       password,
		Name:           name,
		Description:    description,
		ExpirationTime: req.ExpirationTime,
		CreationTime:   creationTime,
	}, nil
}
