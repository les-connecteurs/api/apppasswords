package passwords

import (
	"go.opentelemetry.io/otel/api/global"
)

var tracer = global.Tracer("connecteu.rs/api/apppasswords/passwords")
