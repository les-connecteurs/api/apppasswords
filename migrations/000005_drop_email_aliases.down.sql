-- manage aliases
CREATE TABLE IF NOT EXISTS email."email_aliases" (
  "id"          SERIAL NOT NULL PRIMARY KEY,
  "alias"       TEXT NOT NULL,
  "destination" TEXT NOT NULL
);
