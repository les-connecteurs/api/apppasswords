-- email accounts that have a unexpired password
CREATE OR REPLACE VIEW public.email_users AS
  SELECT DISTINCT
    "username",
    "password"
  FROM public.app_passwords
  WHERE
    ("expires_at" IS NULL
    OR "expires_at" > NOW())
    AND "type" = 'email';

CREATE SCHEMA IF NOT EXISTS email;

-- email accounts that can receive emails
CREATE OR REPLACE VIEW email.email_receiver AS
  SELECT DISTINCT
    "username",
    "password"
  FROM public.email_users;

-- email accounts that can send emails
CREATE OR REPLACE VIEW email.email_sender AS
  SELECT DISTINCT
    "username",
    "password"
  FROM public.email_users
  UNION
  SELECT DISTINCT
    "email" AS username,
    "password"
  FROM public.email_services;
