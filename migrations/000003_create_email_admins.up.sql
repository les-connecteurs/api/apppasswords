-- users that have admin privileges to manage emails
CREATE TABLE IF NOT EXISTS public."email_admins" (
  "id"       SERIAL NOT NULL PRIMARY KEY,
  "username" TEXT NOT NULL,
  "password" TEXT NOT NULL
);

-- all users that can receive emails
CREATE OR REPLACE VIEW email.email_receiver AS
  SELECT DISTINCT
    "username",
    "password"
  FROM public.email_users
  UNION
  SELECT DISTINCT
    "username",
    "password"
  FROM public.email_admins;
