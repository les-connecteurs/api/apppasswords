DROP VIEW IF EXISTS public.email_users;
DROP VIEW IF EXISTS email.email_receiver;
DROP VIEW IF EXISTS email.email_sender;
DROP SCHEMA IF EXISTS email;
