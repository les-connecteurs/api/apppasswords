CREATE OR REPLACE VIEW email.email_receiver AS
  SELECT DISTINCT
    "username",
    "password"
  FROM public.email_users;

DROP TABLE IF EXISTS public."email_admins";
